package Dane;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import Komponenty.Element;
import Komponenty.Wezel;

public class Dane {

	private ArrayList<Wezel> wezly = new ArrayList<Wezel>();
	private ArrayList<Element> elementy = new ArrayList<Element>();

	private File file;
	private Scanner in;

	private int liczbaWezlow;
	private int liczbaElementow;
	
	private double strumienCiepla;
	private double temperaturaOtoczenia;
	private double wspolczynnikKonwekcji;
	
	public Dane() {
		zaladujPlik();
		wczytajDane();
	}

	private void zaladujPlik() {
		try {
			file = new File("dane.txt");
			in = new Scanner(file);
		} catch (FileNotFoundException e) {
			System.out.println("Blad przy wczytywaniu pliku");
		}
	}

	private void wczytajDane() {
		wczytajWezly();
		wczytajStrumienITempOtocz();
		wczytajElementy();
	}

	private void wczytajStrumienITempOtocz() {
		strumienCiepla = in.nextDouble();
		temperaturaOtoczenia = in.nextDouble();
		wspolczynnikKonwekcji = in.nextDouble();
	}

	private void wczytajElementy() {
		liczbaElementow = liczbaWezlow - 1;

		for (int i = 0; i < liczbaElementow; i++) {
			long idPierwszegoWezla = in.nextLong();
			long idDrugiegoWezla = in.nextLong();

			elementy.add(new Element(znajdzWezel(idPierwszegoWezla), znajdzWezel(idDrugiegoWezla),this));
			
			elementy.get(i).setPowierzchnia(in.nextDouble());
			elementy.get(i).setWspolczynnikPrzewodnosciCieplnej(in.nextDouble());
			elementy.get(i).wczytajBrakujaceDane();
			elementy.get(i).stworzMacierzeLokalne();
		}
	}

	private Wezel znajdzWezel(long idWezla) {
		Wezel znaleziono = null;

		for (Wezel w : wezly)
			if (w.getId() == idWezla)
				znaleziono = w;

		return znaleziono;
	}

	private void wczytajWezly() {
		liczbaWezlow = in.nextInt();

		int id;
		double dlugosc;
		int rodzaj;

		for (int i = 0; i < liczbaWezlow; i++) {
			id = in.nextInt();
			dlugosc = in.nextDouble();
			rodzaj = in.nextInt();
			wezly.add(new Wezel(id, dlugosc, rodzaj));
		}
	}

	/*
	 * GETTERS & SETTERS
	 */
	
	public ArrayList<Wezel> getWezly() {
		return wezly;
	}

	public ArrayList<Element> getElementy() {
		return elementy;
	}

	public double getTemperaturaOtoczenia() {
		return temperaturaOtoczenia;
	}
	public double getStrumienCiepla() {
		return strumienCiepla;
	}

	public double getWspolczynnikKonwekcji() {
		return wspolczynnikKonwekcji;
	}
	public int getLiczbaWezlow() {
		return liczbaWezlow;
	}

	public int getLiczbaElementow() {
		return liczbaElementow;
	}
}
