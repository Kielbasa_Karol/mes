package UkladRownan;

import Jama.Matrix;

public class GaussianElimination {
	
	/*
	 * Do rozwiazania ukladu rownian uzywam biblioteki JAMA
	 * Zrodlo:
	 * 		   http://math.nist.gov/javanumerics/jama/
	 * 
	 */

	public static double[] solve(double[][] A, double[] b) {
		Matrix H = new Matrix(A);
		Matrix P = new Matrix(b.length,1);
		for(int i=0;i<b.length;i++)
			P.set(i, 0, -1*b[i]); // mnoze przez -1 poniewaz przenosze macierz P 'na druga strone'
		
		Matrix X = H.solve(P); // [H]{t} = -{P}
		
		double[] x = new double[b.length];
		for(int i=0;i<b.length;i++)
			x[i] = X.get(i, 0);
		return x;
	}
}