package Main;

import Dane.Dane;
import Komponenty.Siatka;

public class Main {

	private static Dane dane;
	private static Siatka siatka;
	
	public static void main(String[] args) {
		dane = new Dane();
		siatka = new Siatka(dane);
	}
}
