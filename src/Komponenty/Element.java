package Komponenty;

import Dane.Dane;

public class Element {

	private Dane dane;
	private Wezel pierwszyWezel;
	private Wezel drugiWezel;

	private double dlugoscElementu; // L
	private double wspolczynnikPrzewodnosciCieplnej; // k
	private double powierzchnia; // S
	private double C; // c = (S*k)/L

	private double HLokalne[][];
	private double PLokalne[];

	public Element(Wezel pierwszyWezel, Wezel drugiWezel, Dane dane) {
		this.pierwszyWezel = pierwszyWezel;
		this.drugiWezel = drugiWezel;
		this.dane = dane;
		
	}

	public void stworzMacierzeLokalne() {
		HLokalne = new double[2][2];
		PLokalne = new double[2];

		if (pierwszyWezel.getRodziaj() == 0) {
			HLokalne[0][0] = C;
			HLokalne[0][1] = -(C);
			
			PLokalne[0] = 0;
		}
		if (drugiWezel.getRodziaj() == 0) {
			HLokalne[1][0] = -(C);
			HLokalne[1][1] = C;
			
			PLokalne[1] = 0;
		}
		if (pierwszyWezel.getRodziaj() == 1) {
			HLokalne[0][0] = C;
			HLokalne[0][1] = -(C);
			
			PLokalne[0] = dane.getStrumienCiepla() * powierzchnia;
		}
		if (drugiWezel.getRodziaj() == 1) {
			HLokalne[1][0] = -(C);
			HLokalne[1][1] = C;
			
			PLokalne[1] = dane.getStrumienCiepla() * powierzchnia;
		}
		if (pierwszyWezel.getRodziaj() == 2) {
			HLokalne[0][0] = C + (dane.getWspolczynnikKonwekcji() * powierzchnia);
			HLokalne[0][1] = -(C);
			
			PLokalne[0] = (-1)*(dane.getTemperaturaOtoczenia()) * (dane.getWspolczynnikKonwekcji()) * (powierzchnia);
		}
		if (drugiWezel.getRodziaj() == 2) {
			HLokalne[1][1] = C + (dane.getWspolczynnikKonwekcji() * powierzchnia);
			HLokalne[1][0] = -(C);
			
			PLokalne[1] = (-1)*(dane.getTemperaturaOtoczenia()) * (dane.getWspolczynnikKonwekcji()) * (powierzchnia);
		}
	}
	
	public void wczytajBrakujaceDane() {
		dlugoscElementu = drugiWezel.getPozycja() - pierwszyWezel.getPozycja();
		C = (powierzchnia * wspolczynnikPrzewodnosciCieplnej) / dlugoscElementu;
	}

	/*
	 * GETTERS & SETTERS
	 */

	public void setWspolczynnikPrzewodnosciCieplnej(double wspolczynnikPrzewodnosciCieplnej) {
		this.wspolczynnikPrzewodnosciCieplnej = wspolczynnikPrzewodnosciCieplnej;
	}

	public void setPowierzchnia(double powierzchnia) {
		this.powierzchnia = powierzchnia;
	}

	public double[][] getHLokalne() {
		return HLokalne;
	}

	public double[] getPLokalne() {
		return PLokalne;
	}

	public Wezel getPierwszyWezel() {
		return pierwszyWezel;
	}

	public Wezel getDrugiWezel() {
		return drugiWezel;
	}
}
