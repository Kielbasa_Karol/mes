package Komponenty;

import Dane.Dane;
import UkladRownan.GaussianElimination;

public class Siatka {

	private Dane dane;

	private double HGlobalne[][];
	private double PGlobalne[];
	private double szukaneTemperatury[];

	public Siatka(Dane dane) {
		this.dane = dane;

		inicjalizacjaTablic();
		swtorzMacierzeGlobalne();
		wypiszMacierzGlobalna();
		liczTemperatury();
	}

	private void liczTemperatury() {
		for(int i =0;i<dane.getLiczbaWezlow();i++)
			szukaneTemperatury[i] = 0;
		szukaneTemperatury = GaussianElimination.solve(HGlobalne, PGlobalne);
		
		wypiszSzukaneTemparatury();
	}

	private void wypiszSzukaneTemparatury() {
		System.out.println();
		System.out.println("Szukane temperatury: ");
		for(int i =0;i<dane.getLiczbaWezlow();i++)
			System.out.println(szukaneTemperatury[i]);
	}

	private void inicjalizacjaTablic() {
		HGlobalne = new double[dane.getLiczbaWezlow()][dane.getLiczbaWezlow()];
		PGlobalne = new double[dane.getLiczbaWezlow()];
		szukaneTemperatury = new double[dane.getLiczbaWezlow()];
	}

	private void swtorzMacierzeGlobalne() {
		for (int i = 0; i < dane.getLiczbaElementow(); i++) {
			/*
			 * HGlobalne [indeks wezla, ktory znajduje sie w arrayliscie elementow pod indeksem i][]
			 * 
			 */
			HGlobalne[dane.getElementy().get(i).getPierwszyWezel().getId()-1][dane.getElementy().get(i).getPierwszyWezel()
					.getId()-1] += dane.getElementy().get(i).getHLokalne()[0][0];
			
			HGlobalne[dane.getElementy().get(i).getDrugiWezel().getId()-1][dane.getElementy().get(i).getDrugiWezel()
					.getId()-1] += dane.getElementy().get(i).getHLokalne()[1][1];
			
			HGlobalne[dane.getElementy().get(i).getPierwszyWezel().getId()-1][dane.getElementy().get(i).getDrugiWezel()
					.getId()-1] += dane.getElementy().get(i).getHLokalne()[0][1];
			
			HGlobalne[dane.getElementy().get(i).getDrugiWezel().getId()-1][dane.getElementy().get(i).getPierwszyWezel()
					.getId()-1] += dane.getElementy().get(i).getHLokalne()[1][0];
			
			PGlobalne[dane.getElementy().get(i).getPierwszyWezel().getId() - 1] += dane.getElementy().get(i).getPLokalne()[0];
			PGlobalne[dane.getElementy().get(i).getDrugiWezel().getId() - 1] += dane.getElementy().get(i).getPLokalne()[1];
		}
	}

	private void wypiszMacierzGlobalna() {
		System.out.println();
		System.out.println("Macierze Globalne H i P:");
		for(int i=0;i<dane.getLiczbaWezlow();i++)
		{
			System.out.print("[");
			for(int j=0;j<dane.getLiczbaWezlow();j++)
			{
				System.out.print(HGlobalne[i][j] + " ");
			}
			System.out.print("]");
			System.out.print("[" + PGlobalne[i]+ "]");
			System.out.println();
		}
	}
}
