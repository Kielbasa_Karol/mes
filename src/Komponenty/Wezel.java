package Komponenty;

public class Wezel {
	
	private int id;
	private double pozycja;
	private int rodziaj;
	
	public Wezel(int id, double pozycja,int rodzaj) {
		this.id = id;
		this.pozycja = pozycja;
		this.rodziaj = rodzaj;
	}
	
	/*
	 * GETTERS & SETTERS
	 */

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPozycja() {
		return pozycja;
	}

	public void setPozycja(double pozycja) {
		this.pozycja = pozycja;
	}

	public int getRodziaj() {
		return rodziaj;
	}

	public void setRodziaj(int rodziaj) {
		this.rodziaj = rodziaj;
	}

}
